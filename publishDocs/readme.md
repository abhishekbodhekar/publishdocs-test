
# publishDocs

publishDocs converts Jupyter Notebooks to [Atlassian Confluence](https://www.atlassian.com/software/confluence)
pages using
[nbconvert](https://github.com/jupyter/nbconvert).



## Features

* Converts most cell inputs and outputs to valid [Confluence storage format](https://confluence.atlassian.com/doc/confluence-storage-format-790796544.html)
* Attaches image outputs (e.g., matplotlib PNGs) to a page, shows them inline,
  and maintains the page-image association in the version history
* Attaches the source notebook to a page, links to it from the page footer, and
  maintains the page-notebook association in the version history


## Usage
1. first of all, you need to setup the environment and install dependencies
    * go the the directory under which  publishDocs is present. Run the following command to install dependencies using pip.
        ```pip install -r requirements.txt```

2. The application requires following resorces to run. 
    * Jupyter notebook
    * Confluence wiki URL (example :- https://example.com/wiki) (dont give / after .../wiki)
    * username
    * password (access token, if password is not allowed in confluence apis)
    * Wiki space name
    * Parent page
    * Title of new page

    All of this things should be passed to cli.py as command line argument as an above described sequence.

    example :- 
    ```python3 -m publishDocs.cli ./my_notebook.ipynb https://example.com/wiki my_username my_password my_space parent_page page_title```

3. You can also delete a page OR list child pages 
    * delete a page -
        * It requires following resources - 
              * Confluence wiki URL (example :- https://example.com/wiki) (dont give / after .../wiki)
              * username
              * password (access token, if password is not allowed in confluence apis)
              * Wiki space name
              * title of page to be deleted

          example - 
          ```python3 -m publishDocs.deletePage https://example.com/wiki my_username my_password my_space page_title```



    * get list of children -
        * It requires following resources - 
              * Confluence wiki URL (example :- https://example.com/wiki) (dont give / after .../wiki)
              * username
              * password (access token, if password is not allowed in confluence apis)
              * Wiki space name
              * title of page

          example - 
          ```python3 -m publishDocs.listPages https://example.com/wiki my_username my_password my_space page_title```





