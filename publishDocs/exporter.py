"""Confluence page exporter that transforms notebook content into Confluence
XML storage format and posts it to an existing page.
"""
import os
import urllib.parse as urlparse
from atlassian import Confluence

import requests

from .filter import sanitize_html
from .markdown import ConfluenceMarkdownRenderer
from .preprocessor import ConfluencePreprocessor
from nbconvert import HTMLExporter
from nbconvert.filters.markdown_mistune import MarkdownWithMath
from traitlets import Bool, List, Unicode
from traitlets.config import Config


class ConfluenceExporter(HTMLExporter):
    """Converts a notebook into Confluence storage format XHTML and the
    notebook binary output cell assets into page attachments, and updates
    a given Confluence page with both.

    Attributes
    ----------
    server: str
        Base URL of the confluence server
    page_id: int
        Page ID to update
    notebook_filename: str
        Local filename of the notebook to be attached to the page

    url: traitlets.Unicode
        Human-readable Confluence page URL to convert to lookup page_id
    username: traitlets.Unicode
        Basic auth username
    password: traitlets.Unicode
        Basic auth password
    generate_toc: bool, optional
        Insert a Confluence table of contents macro at the top of the page (default: True)
    attach_ipynb: traitlets.Bool
        Attach the notebook ipynb to the page and link to it from the page footer (default: True)
    enable_style: traitlets.Bool
        Add the Jupyter base stylesheet to the page (default: True)
    enable_mathjax: traitlets.Bool
        Add MathJax to the page to render equations (default: False)
    """
    url = Unicode(config=True, help='Confluence URL to update with notebook content')
    username = Unicode(config=True, help='Confluence username')
    password = Unicode(config=True, help='Confluence password')
    generate_toc = Bool(config=True, default_value=True, help='Show a table of contents at the top of the page?')
    attach_ipynb = Bool(config=True, default_value=True, help='Attach the notebook ipynb to the page?')
    enable_style = Bool(config=True, default_value=True, help='Add basic Jupyter stylesheet?')
    enable_mathjax = Bool(config=True, default_value=False, help='Add MathJax to the page to render equations?')
    extra_labels = List(config=True, trait=Unicode(), help='List of additional labels to add to the page')

    @property
    def default_config(self):
        overrides = Config({
            'CSSHTMLHeaderPreprocessor': {
                'enabled': False
            },
            'HighlightMagicsPreprocessor': {
                'enabled': False
            },
            'TemplateExporter': {
                'exclude_input_prompt': True,
                'exclude_output_prompt': True
            },
            'TagRemovePreprocessor': {
                'remove_cell_tags': {'nocell', 'no-cell', 'no_cell'},
                'remove_input_tags': {'noinput', 'no-input', 'no_input'},
                'remove_all_outputs_tags': {'nooutput', 'no-output', 'no_output'},
                'enabled': True
            },
            'ExtractOutputPreprocessor': {
                'enabled': True
            },
        })

        c = super(ConfluenceExporter, self).default_config.copy()
        c.merge(overrides)
        return c

    def __init__(self, config, **kwargs):
        config.HTMLExporter.preprocessors = [ConfluencePreprocessor]
        config.HTMLExporter.filters = {
            'sanitize_html': sanitize_html,
        }    
        super(ConfluenceExporter, self).__init__(config=config, **kwargs)
        self._preprocessors[-1].exporter = self

        self.template_path = [os.path.abspath(os.path.dirname(__file__))]
        self.template_file = 'confluence'
        # Must be at least a single character, or the header generator produces
        # an (invalid?) empty anchor tag that trips up bleach during
        # sanitization
        self.anchor_link_text = ' '

   
        self.notebook_filename = None


 

        


    def add_or_update_attachment(self, filename, data, resources):
        """Creates or updates page attachments.

        Parameters
        ----------
        filename: str
            Local filename
        data: bytes
            Data to post
        resources: dict
            Additional nbconvert resources

        Returns
        -------
        request.Response
            Response from the Confluence server
        """
        basename = os.path.basename(filename)
        attachment = resources.get('attachments', {}).get(basename)
        if attachment is None:
            return
        files = {
            'file': (basename, data)
        }
        
        resp = requests.post(attachment.upload_url,
                             headers={
                                 'X-Atlassian-Token': 'nocheck'
                             },
                             files=files,
                             auth=(self.username, self.password))
        resp.raise_for_status()
        
        return resp

    def markdown2html(self, source):
        """Override the base class implementation to force empty tags to be
        XHTML compliant for compatibility with Confluence storage format.
        """
        renderer = ConfluenceMarkdownRenderer(escape=False,
                                              use_xhtml=True,
                                              anchor_link_text=self.anchor_link_text)
        return MarkdownWithMath(renderer=renderer).render(source)

    def from_notebook_node(self, nb, resources=None, **kw):
        """Publishes a notebook to Confluence given a notebook object
        from nbformat.

        Parameters
        ----------
        nb: nbformat.notebooknode.NotebookNode
            Root of a notebook
        resources: dict
            Additional nbconvert resources

        Returns
        -------
        2-tuple
            Published Confluence storage format HTML and nbconvert resources
        """

        if self.notebook_filename is None:
            raise ValueError('only from_filename is supported')

        # Seed resources with option flags
        resources = resources if resources is not None else {}
        resources['generate_toc'] = self.generate_toc
        resources['enable_mathjax'] = self.enable_mathjax
        resources['enable_style'] = self.enable_style

        # Convert the notebook to Confluence storage format, which is XHTML-like
        html, resources = super(ConfluenceExporter, self).from_notebook_node(nb, resources, **kw)
        
        confluence = Confluence(
            url= self.url  ,
            username= self.username ,
            password=  self.password )

        parent_id = confluence.get_page_id(self.space,  self.parent_name)
        status = confluence.update_or_create(parent_id, self.title,html , representation='storage')

        
        '''
        # Update the page with the new content
        self.update_page(self.page_id, html)
        # Add the nbconflux label to the page for tracking
        self.add_label(self.page_id, 'nbconflux')
        # If requested, add any extra labels to the page
        if self.extra_labels:
            for label in self.extra_labels:
                self.add_label(self.page_id, label)
        '''
        
        # Create or update all attachments on the page
        for filename, data in resources.get('outputs', {}).items():
            self.add_or_update_attachment(filename, data, resources)
        
        
        # Create or update the notebook document attachment on the page
        if self.attach_ipynb:
            with open(self.notebook_filename, encoding='utf-8') as f:
                self.add_or_update_attachment(self.notebook_filename, f.read(), resources)
        
        return html, resources

    def from_filename(self, filename,space,parent,title, *args, **kwargs):
        """Publishes a notebook to Confluence given a local notebook filename.

        Parameters
        ----------
        filename: str
            Path to a local ipynb

        Returns
        -------
        2-tuple
            Published Confluence storage format HTML and nbconvert resources
        """
        # Preprocessor needs the filename to attach the notebook source file properly
        # so stash it here for later lookup
        self.notebook_filename = filename
        self.space = space
        self.parent_name = parent
        self.title = title
        confluence = Confluence(
            url= self.url ,
            username= self.username ,
            password= self.password )

        parent_id = confluence.get_page_id(self.space,  self.parent_name)
        status = confluence.update_or_create(parent_id, self.title,"" , representation='storage')

        self.server = self.url
        self.page_id = status["id"]
        return super(ConfluenceExporter, self).from_filename(filename, *args, **kwargs)
