import argparse
import getpass
import os
import sys

from .api import notebook_to_page


def main(argv=None):
    notebook = argv[1]
    url_wiki = argv[2]
    username = argv[3]
    password = argv[4]
    space = argv[5]
    parent = argv[6]
    title = argv[7]
 

    notebook_to_page(notebook, url_wiki, username, password,space,parent,title)

if __name__ == '__main__':
    main(sys.argv)