import argparse
import getpass
import os
import sys
from atlassian import Confluence



def main(argv=None):

    url_wiki = argv[1]
    username = argv[2]
    password = argv[3]
    space = argv[4]
    title = argv[5]

    confluence = Confluence(
            url= url_wiki  ,
            username= username ,
            password=  password )
    page_id = confluence.get_page_id(space, title)
    if (page_id == None):
        return
    status = confluence.remove_page(page_id)
    print(status)



 

    

if __name__ == '__main__':
    main(sys.argv)