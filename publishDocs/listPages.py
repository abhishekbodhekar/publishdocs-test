import argparse
import getpass
import os
import sys
from atlassian import Confluence



def main(argv=None):

    url_wiki = argv[1]
    username = argv[2]
    password = argv[3]
    space = argv[4]
    title = argv[5]
    confluence = Confluence(
            url= url_wiki  ,
            username= username ,
            password=  password )
    page_id = confluence.get_page_id(space, title)
    if (page_id == None):
        return
    names = confluence.get_child_pages(page_id)
    for page in names:
        print(page["title"])




 

    

if __name__ == '__main__':
    main(sys.argv)